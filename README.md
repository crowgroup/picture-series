# Picture Series

App for viewing linked pictures for a given picture ID

**Table of Contents**

[TOC]

## Prerequesties
For non-docker usage you should have installed Node 10 or higher. To build docker image you should have Docker 1.18 or higher.

## Install
- `$ npm i`

## Debug
- `$ npm run dev`

## Deploy
- `$ make`