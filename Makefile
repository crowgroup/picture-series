NAME = crowcloud/picture-series
BRANCH = ${shell git rev-parse --abbrev-ref HEAD}
CROWCLOUD_API = https://dev.crowcloud.xyz

ifeq (${BRANCH}, master)
    BRANCH := latest
    CROWCLOUD_API = https://api.crowcloud.xyz
endif

S3_BUCKET_SYNC ?= picseries.crowcloud.com
DISTRIBUTION_ID ?= E3QMOBAOH4UFI2

default: release

set-api-url:
	sed -i 's#const apiUrl = \x60.*\x60\x3B#const apiUrl = \x60'"${CROWCLOUD_API}"'\x60\x3B#g' scripts.js

build-docker:
	docker build --no-cache -t $(NAME):$(BRANCH) .

build: set-api-url build-docker

push:
	docker push $(NAME):$(BRANCH)

release: build push

clean:
	rm -rf .dist
	mkdir .dist

syncs3:
	aws s3 sync .dist/ s3://${S3_BUCKET_SYNC} --delete --exclude '*.map'
	aws cloudfront create-invalidation --distribution-id ${DISTRIBUTION_ID} --paths /index.html /error.html / '/*'

docker-extract:
	docker run -v ${PWD}/.dist:/tmp/mount -e LOCAL_USER_ID=`id -u` -it ${NAME}:$(BRANCH) cp -r ./ /tmp/mount

release-prod:	clean build-docker docker-extract syncs3