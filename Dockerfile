# Stage 1
FROM node:12 as builder
WORKDIR /usr/share/nginx/html/
COPY styles.css ./styles.css
COPY package*.json ./
RUN npm install
RUN mkdir css-dist
RUN ./node_modules/css-minify/bin/css-minify.js -f ./styles.css
RUN cp ./css-dist/styles.min.css ./styles.css

# Stage 2
FROM nginx:latest

WORKDIR /usr/share/nginx/html/
COPY --from=builder /usr/share/nginx/html/styles.css .
COPY etc/nginx.conf /etc/nginx/conf.d/default.conf
COPY scripts.js ./scripts.js
COPY index.html ./index.html
COPY logo_black.png ./logo_black.png
COPY favicon.ico ./favicon.ico
COPY start.sh /start.sh

CMD ["/bin/sh", "-c", "/start.sh"]
