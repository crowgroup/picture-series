#!/bin/bash
MYIP=`hostname -i`
curl -XPUT -H "Content-Type: application/json" \
 -d '{"id": "'"$SRVID"'", "name": "webcrow", "Address": "'"$MYIP"'","Port": 80, "checks": [{"http": "http://'"$MYIP"':80", "interval": "30s"}]}' \
http://consul:8500/v1/agent/service/register

[[ ! -z "$CROWCLOUD_API" ]] && sed -i 's#const apiUrl = \x60.*\x60\x3B#const apiUrl = \x60'"$CROWCLOUD_API"'\x60\x3B#g' scripts.js

nginx -g 'daemon off;'
