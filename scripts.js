let pictures = [];
let updatedPictures = [];
const apiUrl = `https://api.crowcloud.xyz`;
const updateInterval = 10000;
let pictureId;

(async function init() {
    try {
        // Disable right-click on the page
        document.addEventListener('contextmenu', function(event) {
            event.preventDefault();
            console.log('Right-click detected at:', event.clientX, event.clientY);
            return false;
        });

        // Handle page focus/blur events
        window.addEventListener('blur', function() {
            console.log('Page lost focus');
            document.getElementById('focusOverlay').style.display = 'block';
        });
        
        window.addEventListener('focus', function() {
            console.log('Page regained focus');
            document.getElementById('focusOverlay').style.display = 'none';
        });
        
        pictureId = getPictureIdFromUrl() || '';
        showElementById("isLoading", true);

        pictures = await getPictures();
        if (pictures.length) {
            showElementById('topMenu', true, 'flex');
            showElementById('sliderFooter', true, 'flex');
            buildMac(pictures[0]);
            buildSubElements(0);
            buildSlider();
            buildSendPictureLink();

            $('#pictureCarousel').on('slide.bs.carousel', function (event) {
                buildSubElements(event.to);
            })

            setTimeout(() => {
                checkNewPictures();
            }, updateInterval);
        }
    } catch (e) {
        showElementById("isError", true);
        console.log(e);
    } finally {
        if (!pictures.length) {
            showElementById("isNoRelevantPictures", true);
        }
        showElementById("isLoading", false);
    }
})();

async function checkNewPictures() {
    let interval = setInterval(async () => {
        if (pictures.length == pictures[0]?.pic_total) {
            clearInterval(interval);
            console.log("all pictures downloaded, stop checking");
            return;
        }
        if (moment(pictures[0]?.created).add(15, "minutes").isBefore(moment())) {
            clearInterval(interval);
            console.log("old pictures, stop checking");
            return;
        }

        updatedPictures = await getPictures();
        if (updatedPictures.length !== pictures.length) {
            pictures = updatedPictures;
            buildSlider();
            buildSendPictureLink();
        }

        if (pictures.length == 0) {
            clearInterval(interval);
            console.log("no pictures, stop checking");
            return;
        }
    }, updateInterval);
}

async function getPictures() {
    const response = await fetch(`${apiUrl}/pictures_series/${encodeURIComponent(pictureId)}/`);
    return await response.json();
}

function showElementById(id, flag, displayType = 'block') {
    document.getElementById(id).style.display = flag ? displayType : 'none';
}

function buildMac(picture) {
    const mac = formatMac(picture?.panel_mac);
    document.getElementById("panelMac").innerHTML = `MAC: <b>${mac} ${picture?.panel_foreign_key?.length ? `(${picture?.panel_foreign_key})</b>` : ''}, Zone #${picture.zone + 1} (${picture?.zone_name})`;
}

function formatMac(macString) {
    return macString?.toString( 16 ).match( /.{1,2}/g ).join( ':' ).toUpperCase();
}

function buildSlider() {
    const pictures_holder = document.getElementById("picturesHolder");
    pictures_holder.innerHTML = '';
    for (let i = 0; i < pictures.length; i++) {
        addPicture(i);
    }
}

function buildSubElements(index) {
    document.getElementById("pictureNumber").innerHTML = `<b>${index + 1}</b> / ${pictures.length}`;
    document.getElementById("pictureDate").innerHTML = `${pictures[index].zone_name}`;
    document.getElementById("pictureDate").innerHTML = `${moment(pictures[index].created).utc().format('DD/MM/YYYY HH:m:ss')}`;
    document.getElementById("downloadPicture").setAttribute('href', `${pictures[index].url}?action=download`);
}

function buildSendPictureLink() {
    document.getElementById("sendPictures").setAttribute('href', `mailto:?subject=Picture series from panel ${formatMac(pictures[0].panel_mac)} ${pictures[0].zone_name}&body=${getPicturesString()}`);
}

function addPicture(pictureIdx) {
    const newPicture = document.createElement("div");
    newPicture.className = `carousel-item ${pictureIdx === 0 ? "active" : ""}`;
    newPicture.innerHTML =
        `<div>
            <img src='${pictures[pictureIdx].url}' class="d-block w-100 series" alt='${pictures[pictureIdx].zone_name}'>
        </div>`;
    document.getElementById("picturesHolder").appendChild(newPicture);
}

function playSlider() {
    $('.carousel').carousel('cycle');
}

function pauseSlider() {
    $('.carousel').carousel('pause');
}

function getPicturesString() {
    let result = [];
    pictures.forEach((picture) => {
        result.push(picture.url);
    });
    return result.join('%0D%0A');
}

function getPictureIdFromUrl() {
    const match = RegExp('#!([^.]*.jpg)').exec(window.location.href);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}
